package dataoutput

import (
	"fmt"

	"github.com/johnfercher/maroto/pkg/consts"
	"github.com/johnfercher/maroto/pkg/pdf"
	"github.com/johnfercher/maroto/pkg/props"

	fpdf "github.com/jung-kurt/gofpdf"
)

func createPdf(dataFormatting DataFormatting, data [][]any, pageSize string, orientation string, pathOut string) error {
	pdf := fpdf.New(orientation, "mm", pageSize, "")
	pdf.SetFont("Arial", "", 12)

	pdf.SetTopMargin(30)

	pdf.SetHeaderFuncMode(func() {
		_, mTop, _, _ := pdf.GetMargins()
		pdf.SetY(mTop - float64(dataFormatting.RowHeight))
		for _, col := range dataFormatting.Columns {
			pdf.Cell(float64(col.Width), float64(dataFormatting.RowHeight), fmt.Sprintf(col.GetFormatString(), col.Name))
		}
	}, true)

	pdf.AddPage()

	for _, row := range data {
		for colIndex, dataCell := range row {
			colum := dataFormatting.Columns[colIndex]
			pdf.Cell(float64(colum.Width), float64(dataFormatting.RowHeight), fmt.Sprintf(colum.GetFormatString(), dataCell))
		}
		pdf.Ln(-1)
	}

	return pdf.OutputFileAndClose(pathOut)
}

func createPdfmaroto(dataFormatting DataFormatting, data [][]any, pageSize string, orientation string) ([]byte, error) {
	m := pdf.NewMaroto(consts.Orientation(orientation), consts.PageSize(pageSize))

	for _, row := range data {

		m.Row(float64(dataFormatting.RowHeight), func() {

			for colIndex, dataCell := range row {
				header := dataFormatting.Columns[colIndex]

				m.Col(uint(header.Width), func() {

					formatStr := "%v"
					if header.FormatString != "" {
						formatStr = header.FormatString
					}

					m.Text(fmt.Sprintf(formatStr, dataCell), props.Text{
						// Top:         12,
						// Size:        20,
						// Extrapolate: true,
					})

				})
			}
			// m.ColSpace(4)
		})
	}

	// m.Row(40, func() {
	// 	m.Col(4, func() {
	// 		m.Text("TEST", props.Text{
	// 			Top:         12,
	// 			Size:        20,
	// 			Extrapolate: true,
	// 		})

	// 	})
	// 	m.Col(4, func() {
	// 		m.Text("Gopher International Shipping, Inc.", props.Text{
	// 			Top:         12,
	// 			Size:        20,
	// 			Extrapolate: true,
	// 		})
	// 		m.Text("1000 Shipping Gopher Golang TN 3691234 GopherLand (GL)", props.Text{
	// 			Size: 12,
	// 			Top:  22,
	// 		})
	// 	})
	// 	m.ColSpace(4)
	// })

	// m.Line(10)

	// m.Row(40, func() {
	// 	m.Col(4, func() {
	// 		m.Text("João Sant'Ana 100 Main Street Stringfield TN 39021 United Stats (USA)", props.Text{
	// 			Size: 15,
	// 			Top:  12,
	// 		})
	// 	})
	// 	m.ColSpace(4)
	// 	m.Col(4, func() {
	// 		m.QrCode("https://github.com/johnfercher/maroto", props.Rect{
	// 			Center:  true,
	// 			Percent: 75,
	// 		})
	// 	})
	// })

	// m.Line(10)

	// m.Row(100, func() {
	// 	m.Col(12, func() {
	// 		_ = m.Barcode("https://github.com/johnfercher/maroto", props.Barcode{
	// 			Center:  true,
	// 			Percent: 70,
	// 		})
	// 		m.Text("https://github.com/johnfercher/maroto", props.Text{
	// 			Size:  20,
	// 			Align: consts.Center,
	// 			Top:   65,
	// 		})
	// 	})
	// })

	// m.SetBorder(true)

	// m.Row(40, func() {
	// 	m.Col(6, func() {
	// 		m.Text("CODE: 123412351645231245564 DATE: 20-07-1994 20:20:33", props.Text{
	// 			Size: 15,
	// 			Top:  14,
	// 		})
	// 	})
	// 	m.Col(6, func() {
	// 		m.Text("CA", props.Text{
	// 			Top:   1,
	// 			Size:  85,
	// 			Align: consts.Center,
	// 		})
	// 	})
	// })

	// m.SetBorder(false)

	out, err := m.Output()
	if err != nil {
		return nil, err
	}
	return out.Bytes(), err

	// err := m.OutputFileAndClose("internal/examples/pdfs/zpl.pdf")
	// if err != nil {
	// 	fmt.Println("Could not save PDF:", err)
	// 	os.Exit(1)
	// }

	// end := time.Now()
	// fmt.Println(end.Sub(begin))
}
