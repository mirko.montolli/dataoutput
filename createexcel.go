package dataoutput

import (
	excelize "github.com/xuri/excelize/v2"
)

func convWidth(value int) float64 {
	// fa := (1 / (72.0 / 25.4))
	return float64(value) * 0.5
}

func convHeight(value int) float64 {
	return float64(value) * 3
}

func createExcel(dataFormatting DataFormatting, data [][]any, pathOut string) error {
	f := excelize.NewFile()

	sheetName := "Sheet1"
	styleDef, err := f.NewStyle(`{"font":{"family":"Arial","size":12}}`)
	if err != nil {
		return err
	}

	// HEADER
	for colIndex, col := range dataFormatting.Columns {
		f.SetRowHeight(sheetName, 1, convHeight(dataFormatting.RowHeight))
		nameCol, err := excelize.ColumnNumberToName(colIndex + 1)
		if err != nil {
			return err
		}
		err = f.SetColWidth(sheetName, nameCol, nameCol, convWidth(col.Width))
		if err != nil {
			return err
		}
	}

	// COLUMNS STYLE
	for colIndex, col := range dataFormatting.Columns {
		x := colIndex + 1
		y := 1

		cellIndex, err := excelize.CoordinatesToCellName(x, y)
		if err != nil {
			return err
		}
		f.SetCellValue(sheetName, cellIndex, col.Name)
		f.SetCellStyle(sheetName, cellIndex, cellIndex, styleDef)
	}

	// ROWS
	for rowIndex, row := range data {
		for colIndex, dataCell := range row {
			x := colIndex + 1
			y := rowIndex + 1 + 1

			f.SetRowHeight(sheetName, y, convHeight(dataFormatting.RowHeight))

			cellIndex, err := excelize.CoordinatesToCellName(x, y)
			if err != nil {
				return err
			}
			f.SetCellValue(sheetName, cellIndex, dataCell)
			f.SetCellStyle(sheetName, cellIndex, cellIndex, styleDef)
		}
	}

	// Set value of a cell.

	// Save spreadsheet by the given path.
	if err := f.SaveAs(pathOut); err != nil {
		return err
	}
	return nil
}
