package dataoutput

import (
	"errors"
	"fmt"
	"os"
	"testing"
)

func TestOutData(t *testing.T) {

	ex := DataFormatting{}
	ex.RowHeight = 8
	ex.Columns = append(ex.Columns, ColumnDef{Width: 50, Name: "Colonna 1"})
	ex.Columns = append(ex.Columns, ColumnDef{Width: 50, Name: "Colonna 2"})
	ex.Columns = append(ex.Columns, ColumnDef{Width: 50, Name: "Colonna 3"})
	ex.Columns = append(ex.Columns, ColumnDef{Width: 50, Name: "Colonna 4"})

	data := [][]any{}

	for i := 1; i <= 200; i++ {
		data = append(data, []any{i, fmt.Sprintf("TESTO %v", i), "PROVA", "XXXS"})
	}

	var err error
	fileExcel := "c:\\tmp\\out.xlsx"
	filePdf := "c:\\tmp\\out.pdf"

	if _, err := os.Stat(fileExcel); !errors.Is(err, os.ErrNotExist) {
		err = os.Remove(fileExcel)
		if err != nil {
			t.Fatalf(`Impossibile cancellare il file %s`, fileExcel)
		}
	}

	if _, err := os.Stat(filePdf); !errors.Is(err, os.ErrNotExist) {
		err = os.Remove(filePdf)
		if err != nil {
			t.Fatalf(`Impossibile cancellare il file %s`, filePdf)
		}
	}

	err = CreateExcel(ex, data, fileExcel)

	if err != nil {
		t.Fatalf(`EXCEL %v`, err)
	}

	err = CreatePdf(ex, data, "A4", "L", filePdf)
	if err != nil {
		t.Fatalf(`PDF %v`, err)
	}

}
