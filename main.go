package dataoutput

import (
	"fmt"
	"path"
)

func checkDataStruct(dataFormatting DataFormatting, data [][]any) error {
	if len(data) == 0 {
		return fmt.Errorf("Nessun dato da stampare")
	}

	numColonneDati := len(data[0])
	numColonneDef := len(dataFormatting.Columns)
	if numColonneDati > numColonneDef {
		return fmt.Errorf("Specificare la formattazione per tutte le colonne")
	}
	if numColonneDati < numColonneDef {
		return fmt.Errorf("Numero di definizione di colonne maggiore del dataset")
	}

	return nil
}

// dataFormatting Formattazzione della pagina
// data dati in formato righe/colonne
// pageSize: "A3", "A4", "A5", "Letter", "Legal", or "Tabloid"
// orientation: "P","L" (Portrait, Landscape)
// tutte le dimensioni sono espresse in mm
func CreatePdf(dataFormatting DataFormatting, data [][]any, pageSize string, orientation string, pathOut string) error {
	err := checkDataStruct(dataFormatting, data)
	if err != nil {
		return err
	}

	err = createPdf(dataFormatting, data, pageSize, orientation, pathOut)
	if err != nil {
		return err
	}
	// value, err := createPdf(dataFormatting, data, pageSize, orientation, pathOut)
	// if err != nil {
	// 	return err
	// }
	// err = ioutil.WriteFile(pathOut, value, fs.ModeExclusive)
	// if err != nil {
	// 	return err
	// }

	return nil
}

func CreateExcel(dataFormatting DataFormatting, data [][]any, filename string) error {
	err := checkDataStruct(dataFormatting, data)
	if err != nil {
		return err
	}

	if path.Ext(filename) != ".xlsx" {
		return fmt.Errorf("L'estensione deve essere di tipo xlsx")
	}

	err = createExcel(dataFormatting, data, filename)
	if err != nil {
		return err
	}
	return nil
}

type DataFormatting struct {
	Columns   []ColumnDef
	RowHeight int
}

type Align int

const (
	TOP    Align = 0
	LEFT         = 1
	BOTTOM       = 2
	RIGHT        = 3
)

type FormatStyle struct {
	BorderWidth int
	BorderColor string // html
	BorderSides []Align
}

type ruleSpanDef func(rowIndex int, data [][]any) int

type HeaderColumn struct {
	Title string
	Style FormatStyle
	Align Align
}

type ColumnDef struct {
	Name           string
	Header         HeaderColumn
	Width          int
	RowSpanEnabled bool
	RowSpanRule    ruleSpanDef
	ColSpanEnabled bool
	ColSpanRule    ruleSpanDef
	Style          FormatStyle
	Align          Align
	FormatString   string
}

func (cd ColumnDef) GetFormatString() string {
	formatStr := "%v"
	if cd.FormatString != "" {
		formatStr = cd.FormatString
	}
	return formatStr
}
